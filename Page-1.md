SCSS/SASS (CSS pre-porcessers)
==============================

Now a days in many projects we use CSS pre-processors.A preprocessor allows additional leverage over CSS by providing additional syntax that delivers the following advantages:

1. Nested syntax
2. Ability to define variables
3. Ability to define functions
4. Mathematical functions
4. Operational functions (such as “lighten” and “darken”)
5. Joining of multiple files
6. Optimzied CSS output.

Basics: [http://sass-lang.com/](http://sass-lang.com/) .

Install: SASS/SCSS [http://sass-lang.com/install](http://sass-lang.com/install) , requires ruby.

Basic Idea: [http://sass-lang.com/guide](http://sass-lang.com/guide)

Difference b/w .sass & .scss
  Instead of brackets and semicolons(used in SCSS), SASS uses the indentation of lines to specify blocks. Advised to use SCSS :-).
