1. SCSS Comments

 Sass supports standard multiline CSS comments with /* */, as well as single-line comments with //.
The multiline comments are preserved in the CSS output where possible, while the single-line comments are removed.

2. Nested Rules

```
$brand-font: red;
.container {
  .div {
    color: $brand-font;
  }
}
```
is compiled to

```
.container .div{
  color: red;
}
```

Parent Selector can be reffernced using &

```
a {
  text-decoration: none;

  &:hover {
   text-decoration: underline;
  }
}
```
is compiled to

```
a { text-decoration: none;}
a:hover { text-decoration: underline;}
```